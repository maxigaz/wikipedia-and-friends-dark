# Wikipedia and Friends Dark Theme

A dark theme for Wikipedia and [other Wikimedia projects](https://wikimediafoundation.org/our-work/wikimedia-projects/).

This style is written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format, specifically for [Stylus](https://add0n.com/stylus.html), although it might be compatible with other style managers such as [xStyle](https://github.com/FirefoxBar/xStyle).

## Install with Stylus

If you have Stylus installed, click on one of the banners below and a new window of Stylus should open, asking you to confirm to install the style.

**Stable version** (updated less frequently, with version number increase): coming soon™  
**Development version** (updated after every commit, reinstall it manually to see the latest changes): [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/wikipedia-and-friends-dark/raw/master/wikipedia-and-friends-dark.user.css)

## Submitting issues

Before you open a new issue ticket, please, make sure you’ve done the following:

- Read the known issues listed below.
- Install the latest development version. (Open the style from [here](https://gitlab.com/maxigaz/wikipedia-and-friends-dark/raw/master/wikipedia-and-friends-dark.user.css). If you have installed it earlier, tell Stylus to reinstall it, overriding the version you currently have.)

In the issue description, include the following:

- An example URL to the page you’re experiencing the problem on or provide step by step instructions on how to reproduce it.
- The version of your web browser and the userstyle.
- In addition, including a screenshot or screencast of the problem is also helpful.

## Known issues

### Some icons are darker than they should be

I’m not sure how to fix them at the moment. Suggestions are welcome!

## Credits

The userstyle is inspired by [Arc Dark](https://github.com/horst3180/Arc-theme), although it doesn't follow all its design choices.

## More userstyles from me

If you like this userstyle, check out the list of all styles created/maintained by me [here](https://gitlab.com/maxigaz/userstyles).
